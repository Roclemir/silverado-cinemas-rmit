// Function to play the trailer for the appropriate movie using 
// featherlight plugin
function playTrailer( title )   {
    switch ( title )  {
        // Trailers for the now showing page
        case "ALEX AND EVE":
            $.featherlight( $( '#alexEveTrailer' ) );
            break;
        case "THE GOOD DINOSAUR":
            $.featherlight( $( '#goodDinosaurTrailer' ) );
            break;
        case "TAMASHA":
            $.featherlight( $( '#tamashaTrailer' ) );
            break;
        case "SPECTRE":
            $.featherlight( $( '#spectreTrailer' ) );
            break;

        // Trailers for the coming soon page
        case "SISTERS":
            $.featherlight( $( '#sistersTrailer' ) );
            break;
        case "THE PEANUTS MOVIE":
            $.featherlight( $( '#peanutsTrailer' ) );
            break;
        case "CAROL":
            $.featherlight( $( '#carolTrailer' ) );
            break;
        case "CREED":
            $.featherlight( $( '#creedTrailer' ) );
    }
}