/*jslint browser: true*/
/*global $, jQuery, alert*/

// This brings up the purchase ticket page
function callToAction() {
    "use strict";
    $( '.current' ).removeClass( 'current' );
    $( 'ul > li:nth-child(4)' ).addClass( 'current' );
    $( '.mainBody > div' ).fadeOut( 'fast' );
    $( '#buy' ).fadeIn( 'fast' );
} // End callToAction

// This function is for highlighting the correct navigation bar item
function menuHighlight( item ) {
    "use strict";
    // Drop existing highlighting
    $( '.current' ).removeClass( 'current' );

    // Add highlighting to newly selected item
    $( item ).parent().addClass( 'current' );
} // End menuHighlight


// This function changes the main content of the page
function changeContent( item ) {
    "use strict";
    switch ( item ) {
        // These first few correspond to nav bar items
        case "HOME":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#home' ).fadeIn( 'fast' );
            break;
        case "NOW SHOWING":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#showing' ).fadeIn( 'fast' );
            break;
        case "COMING SOON":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#coming' ).fadeIn( 'fast' );
            break;
        case "RESERVE TICKETS":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#buy' ).fadeIn( 'fast' );
            break;
        case "SHOPPING CART":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#cart' ).fadeIn( 'fast' );
            break;
        case "CONTACT US":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#contacts' ).fadeIn( 'fast' );
            break;
            
        // These next cases correspond to the individual movie pages
        // Now showing movies
        case "romcomLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#alexEve' ).fadeIn( 'fast' );
            break;
        case "childLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#theGoodDino' ).fadeIn( 'fast' );
            break;
        case "tamashaLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#tamasha' ).fadeIn( 'fast' );
            break;
        case "spectreLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#spectre' ).fadeIn( 'fast' );
            break;
        // Coming soon movies
        case "csForeignLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#sisters' ).fadeIn( 'fast' );
            break;
        case "csChildLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#peanuts' ).fadeIn( 'fast' );
            break;
        case "csRomcomLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#carol' ).fadeIn( 'fast' );
            break;
        case "csActionLink":
            $( '.mainBody > div' ).fadeOut( 'fast' );
            $( '#creed' ).fadeIn( 'fast' );
            break;
    }
}

// This function highlights the menu item that the mouse is 
// hovering over
function hoverHighlight( item )   {
    "use strict";
    
    if( !$( item ).parent().hasClass( 'current' ) ) {
        $( item ).parent().addClass( 'hover' );
    }
}
// The above and below functions while similar require seperation as the       
// are linked to different event handlers. If the above function was used 
// to drop the 'hover' class, then the class would only be dropped at the
// time something else was highlighted, and not when the mouse is moved 
// off the hovered over item.

// Unhighlights the menu item onmouseout
function hoverUnHighlight( item ) {
    "use strict";
    
    if( $( item ).parent().hasClass( 'hover' ) )  {
        $( item ).parent().removeClass( 'hover' );
    }
}

// This checks if post variables exist when the page is loaded and if it does
// it changes the initially displayed screen to the cart.
function checkSession() {
    
}