// These variables change depending on day/time/number of seats
var adultCost, childCost, concCost, premAdust, premChild, beanCost, seatOpts = 10;

$(function() {

    // The next six lines highlights links that are hovered over
    $( '.callToAction, .trailer, .movieImg' ).mouseover( function() {
        $( this ).addClass( 'flare' );
    });
    $( '.callToAction, .trailer, .movieImg' ).mouseout( function() {
        $( this ).removeClass( 'flare' );
    });
    
    // For the new layout display on the buy tickets page
    $( '#layoutLink' ).on( 'click', '.layout', function() {
        $.featherlight( $( '.layout' ) );
    });
    
////////////////////////////////////////////////////
// The following function is in bookmark.js ////////
////////////////////////////////////////////////////
    $('#bookmark-this').click(function(e) {
        bookmark(e);
    });
    
//////////////////////////////////////////////////////////
// Form validation code from the form validation plugin //
//////////////////////////////////////////////////////////
    // $(".detailForm").validate();

////////////////////////////////////////////////////
// These functions are in the purchaseTix.js file //
////////////////////////////////////////////////////
    // This opens the seating selection area and adds more info 
    // to the right hand side
    $( '.movieField' ).on('change','#selectedMovie',function(){
        var newDate = new Date( $( '#datePicker' ).val() );
        openTixArea( newDate.getDay(), $('#selectedMovie' ).val() );
    });

    // When a date is selected
    $( '#datePicker' ).change( function() {
        var newDate = new Date( $( '#datePicker' ).val() );
        movieSelection( newDate );
        if ( $( '.tixField' ).css( 'display' ) === "block" )
            $( '.tixField' ).fadeOut();
    });
    
    $( '.tickets' ).change( function()  {
        calc( this );
        $( '.submitPurch' ).fadeIn();
    });
    
    $( '.tixField' ).on( 'change', '#sAdult, #sChild, #sConc, #pAdult, #pChild, #bean1, #bean2, #bean3', function() {
        checkZero();
    });
    

    
//////////////////////////////////////////////////////////////
// The following functions are in the contentChange.js file //
//////////////////////////////////////////////////////////////
    
    // This is the event listener function for changing content and
    // highlighting
    $( 'ul' ).on( 'click', 'a', function() {
        menuHighlight( $( this ) );
        changeContent( $( this ).text() );
    });
    $( 'ul' ).on( 'mouseover', 'a', function() {
        hoverHighlight( $( this ) );
    });
    $( 'ul' ).on( 'mouseout', 'a', function()   {
        hoverUnHighlight( $( this ) );
    });

    // This function shows the buy tickets page anytime a call
    // to action buttom is pressed
    $( '.callToAction' ).click( function() {
          callToAction();
    });
    
    $( '.moreDetails' ).click( function() {
        var linkTitle = $( this ).attr( 'class' ).split( ' ' )[0];
        changeContent( linkTitle );
    });
    
    checkSession();

/////////////////////////////////////////////////////////
// The following functions are in the trailers.js file //
/////////////////////////////////////////////////////////
    
    // code to fire Featherlight plugin on click
    $( '.trailer' ).click( function() {
        var title = $( this ).parent().find( 'h3' ).text();
        playTrailer( title );
    });
    
//////////////////////////////////////////
// Discount client side string-checking //
//////////////////////////////////////////
    $("#txtboxToFilter").keydown(function(event) {
        
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
                 
            }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey ||(event.keyCode == 45 )) {   
                event.preventDefault(); 
            }   
        
                 
        }

        if(  $("#txtboxToFilter").val().length == 5 )
       {
            event.target.value = event.target.value + "-";
       }
        if(  $("#txtboxToFilter").val().length == 11 )
       {
            event.target.value = event.target.value + "-";
       }
    });
    
}); // End document.ready()
