// This calculates and displays the cost of the tickets.
function calc( seats )   {
    var type = $( seats ).attr( 'id' ), amount, numOf = $( seats ).val(), label;
    switch ( type ) {
        case "sAdult":
            label = numOf + " Adult Tickets: $";
            amount = (adultCost * numOf).toFixed(2);
            break;
        case "sChild":
            label = numOf + " Child Tickets Price: $";
            amount = (childCost * numOf).toFixed(2);
            break;
        case "sConc":
            label = numOf + " Concession Tickets: $";
            amount = (concCost * numOf).toFixed(2);
            break;
        case "pAdult":
            label = numOf + " First Class Adult Tickets: $";
            amount = (premAdust * numOf).toFixed(2);
            break;
        case "pChild":
            label = numOf + " First Class Child Tickets: $";
            amount = (premChild * numOf).toFixed(2);
            break;
        case "bean1": label = numOf + " Beanbags for 1: $";
            amount = (beanCost * numOf).toFixed(2);
            break;
        case "bean2": label = numOf + " Beanbags for 2: $";
            amount = (beanCost * numOf).toFixed(2);
            break;
        case "bean3": label = numOf + " Beanbags for 3: $";
            amount = (beanCost * numOf).toFixed(2);
            break;
    } // End swicth
    addMoney( amount, type, label );
    if ( amount === "0.00" ) {
        removeLabel( type );
    } // End if
} // End calc()

function removeLabel( type )    {
    $( '.' + type ).remove();
    if ( !$( '.subtotal' ).length > 0 )
        $( '.total' ).fadeOut();
    if ( $( '.total' ).val() === "0.00" )
        resetInfo();
} // End removeLabel()

// Helper function for calc()
function addMoney( amount, type, label )   {
    if ( $( '.' + type ).length > 0 ) {
        $( '.' + type ).replaceWith( '<label class="' + type + '">' + label + '<input class="subtotal selectInfoFields" readonly value="' + amount + '"></label>' );
    } else {
        $( '.total' ).before( '<label class="' + type + '">' + label + '<input class="subtotal selectInfoFields" readonly value="' + amount + '"></label>' );
    }
    
    addTotal();
} // End addMoney()

// Another helper function - I'm trying to make this more modular and abstract
function addTotal() {
    var total = 0;
    $( '.subtotal' ).each( function()   {
        total += parseFloat( $( this ).val() );
    });
    total = total.toFixed(2);
    $( '.total' ).replaceWith( '<label class="total">TOTAL: $<input type="text" class="selectInfoFields" readonly value="' + total + '"></label>' );
    $( '.submitPurch' ).before( '<input class="hide" type="text" name="price" value="' + total + '">');
} // End addTotal()
// This function displays the movie selection box and allows the user to 
// only select movies available for the chosen date.
function movieSelection( chosenDate ) {
    // Bring in next section and adjust options according to day of week
    $( '.movieField' ).fadeIn();
    switch ( chosenDate.getDay() ) {
        case 0:
        case 6:
            $( '#selectTamasha, #selectDino, #selectAlexEve, #selectSpectre' ).fadeIn();
            break;
        case 1:
        case 2:
            $( '#selectSpectre' ).fadeOut();
            $( '#selectTamasha, #selectDino, #selectAlexEve' ).fadeIn();
            break;
        case 3:
        case 4:
        case 5:
            $( '#selectTamasha' ).fadeOut();
            $( '#selectDino, #selectAlexEve, #selectSpectre' ).fadeIn();
            break;
    } // End switch
    
    // Display the chosen date on the right side
    // I've elected to add fields here for later use. This makes replacing
    // html on a change easier (becuase you can't replace something thats
    // not there) 
    var date = chosenDate.getDate(), month = chosenDate.getMonth() + 1, 
        year = chosenDate.getFullYear();
    $( '.buy.R > p' ).remove();
    
    // To prevent the following showing up multiple times when the date is changed,
    // we need to first test if they exist and get rid of them before creating them
    // again.
    resetInfo();
    $( '.buy.R' ).append( '<p>Date: ' + date + '/' + month + '/' + year + '</p>' );
    $( '.buy.R' ).append( '<p class="movieToWatch"></p>' );
    $( '.buy.R' ).append( '<p class="movieTime"></p>' );
    $( '.buy.R' ).append( '<label class="total"></label>' );
    
    // First, add numbers to drop down boxes
    addNumbers();
} // End movieSelection()

// This function makes the seat selection visible, and once a movie is selected,
// the movie name and time is displayed on the Selected Info section.
function openTixArea( day, movie ){
    var time, dayName;
    $( '.tixField' ).fadeIn();
    
    // Nested switch statements - awesome right? I've read somewhere they can be 
    // hard to follow, so here's a rundown:
    // The outer switch statement is about the movie. It adds the movie name 
    // to the right hand side of the web page.
    // The inner (or nested) switch statement is to work out what day it is on.
    // This information is required so that we can find the time of the movie,
    // which is required for the form submission, and its added to the right hand 
    // display.
    
    switch ( movie )    {
        case "AC":
            $( '.movieToWatch' ).replaceWith( '<p class="movieToWatch">Spectre</p>' );
            adultCost = 18;
            childCost = 12;
            concCost = 15;
            premAdust = 30;
            premChild = 25;
            beanCost = 30;
            time = 9; // No switch statement required - it only plays at 9pm.
            break;
        case "CH":
            $( '.movieToWatch' ).replaceWith( '<p class="movieToWatch">The Good Dinosaur</p>' );
            switch ( day )  {
                case 1:
                case 2:
                    time = 1;
                    adultCost = 12;
                    concCost = 10;
                    childCost = 8;
                    premAdust = 25;
                    premChild = 20;
                    beanCost = 20;
                    break;
                case 3:
                case 4:
                case 5:
                    time = 6;
                    adultCost = 18;
                    childCost = 12;
                    concCost = 15;
                    premAdust = 30;
                    premChild = 25;
                    beanCost = 30;
                    break;
                case 0:
                case 6:
                    time = 12;
                    adultCost = 18;
                    childCost = 12;
                    concCost = 15;
                    premAdust = 30;
                    premChild = 25;
                    beanCost = 30;
                    break;
            } // End inner switch
            break;
        case "AF":
            $( '.movieToWatch' ).replaceWith( '<p class="movieToWatch">Tamasha</p>' );
            switch ( day )  {
                case 1:
                case 2:
                    time = 6;
                    adultCost = 12;
                    concCost = 10;
                    childCost = 8;
                    premAdust = 25;
                    premChild = 20;
                    beanCost = 20;
                    break;
                case 0:
                case 6:
                    time = 3;
                    adultCost = 18;
                    childCost = 12;
                    concCost = 15;
                    premAdust = 30;
                    premChild = 25;
                    beanCost = 30;
                    break;
            } // End inner switch
            break;
        case "RC":
            $( '.movieToWatch' ).replaceWith( '<p class="movieToWatch">Alex &amp; Eve</p>' );
            switch ( day )  {
                case 1:
                case 2:
                    time = 9;
                    adultCost = 12;
                    concCost = 10;
                    childCost = 8;
                    premAdust = 25;
                    premChild = 20;
                    beanCost = 20;
                    break;
                case 3:
                case 4:
                case 5:
                    time = 1;
                    adultCost = 12;
                    concCost = 10;
                    childCost = 8;
                    premAdust = 25;
                    premChild = 20;
                    beanCost = 20;
                    break;
                case 0:
                case 6:
                    time = 6;
                    adultCost = 18;
                    childCost = 15;
                    concCost = 12;
                    premAdust = 30;
                    premChild = 25;
                    beanCost = 30;
                    break;
            } // End inner switch
            
            break;
    } // End outer switch

    $( '.movieTime' ).replaceWith( '<p class="movieTime">Time: ' + time + 'pm</p>');
    $( '.submitPurch' ).before( '<input class="hide" type="text" name="time" value="' + time + '">');
    switch ( day )   {
        case 0:
            dayName = "Sunday";
            break;
        case 1:
            dayName = "Monday";
            break;
        case 2:
            dayName = "Tuesday";
            break;
        case 3:
            dayName = "Wednesday";
            break;
        case 4:
            dayName = "Thursday";
            break;
        case 5:
            dayName = "Friday";
            break;
        case 6:
            dayName = "Saturday";
            break;
    } // End switch
    $( '.dayOfMovie' ).attr( 'value', dayName );
    // $( '.submitPurch' ).before( '<input type="hidden" name="booking[day]" value="' + dayName + '">');
    
} // End openTixArea()

// Was quicker to write this than to put each option in manually.
function addNumbers()   {
    if ( !( $( '.seatOpt' ).length > 0 ) )    {
        $( '.tixField' ).find( 'select' ).each( function() {
            for ( var i = 0; i <= seatOpts; ++i ) {
                if ( i === 0 )  {
                    $( this ).append( '<option selected class="seatOpt">' + i + '</option>' );
                } else {
                    $( this ).append( '<option class="seatOpt">' + i + '</option>' );
                } // End if
            } // End for loop
        }); // End anonymous function
    } // End if exists
} // End addNumbers()

// Reset the bookings info area
function resetInfo()    {
    $( '.submitPurch' ).fadeOut;
    if ( $( '.total').length > 0 )    {
        $( '.movieToWatch' ).remove();
        $( '.movieTime' ).remove();
        $( '.total' ).remove();
    } // End if
    if ( $( '.sAdult' ).length > 0 )
        $( '.sAdult' ).remove();
    if ( $( '.sChild' ).length > 0 )
        $( '.sChild' ).remove();
    if ( $( '.sConc' ).length > 0 )
        $( '.sConc' ).remove();
    if ( $( '.pAdult' ).length > 0 )
        $( '.pAdult' ).remove();
    if ( $( '.pChild' ).length > 0 )
        $( '.pChild' ).remove();
    if ( $( '.bean1' ).length > 0 )
        $( '.bean1' ).remove();
    if ( $( '.bean2' ).length > 0 )
        $( '.bean2' ).remove();
    if ( $( '.bean3' ).length > 0 )
        $( '.bean3' ).remove();
    
    // Remove seating options
    for ( var i = 0; i <= seatOpts; ++i ) {
        $( '.seatOpt' ).remove();
    } // End for loop
    
    // Reset selected movie
    $( '#selectedMovie' ).val( '#default' );
} // End resetInfo1()

function checkZero()    {
    if (   $( '#sAdult'  ).val() === '0' 
        && $( '#sChild'  ).val() === '0' 
        && $( '#sConc'   ).val() === '0'
        && $( '#pAdult'  ).val() === '0'
        && $( '#pChild'  ).val() === '0'
        && $( '#bean1'   ).val() === '0'
        && $( '#bean2'   ).val() === '0'
        && $( '#bean3'   ).val() === '0' )
        
    {
        $( '.submitPurch' ).fadeOut();
    }
}