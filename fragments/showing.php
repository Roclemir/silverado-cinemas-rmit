<!-- Referrences:
Movie pictures sourced from Hoytes website:
Hoyts, (n.d.). Hoyts. 
[online] Available at: http://www.hoyts.com.au/ 
[Accessed 23 Dec. 2015].

Movie details sourced from IMDB website:
IMDb, (n.d.). IMDb - Movies, TV and Celebrities. 
[online] Available at: http://www.imdb.com/ 
[Accessed 23 Dec. 2015].
-->


<!-- This page fragment displays all currently showing movies, 
one in each quadrant -->
<div class="quadrants">
    
    <!-- The second level div tag classes T L R and B relate to (T)op, (L)eft, 
        (R)ight and (B)ottom. So a div with the classes T and L is in the top 
        left of the page. These classes are used in the CSS to get the desired
        effect. -->
    
    <!-- Each quadrant is layed out is like this:
        Name of movie at the top,
        Div containing the cover picture on the left 
            and some basic details in
        a few divs on the right,
        A one-liner blurb and
        A link to watch the trailer.
    -->
    
        
    
    <div class="quad T L">
        <?php $thisMovie = $romcom; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="romcomLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Alex and Eve Title Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="romcomLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="alexEveTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
        <a class="callToAction">BUY TICKETS</a>
    </div>
    
    <div class="quad T R">
        <?php $thisMovie = $child; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="childLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Alex and Eve Title Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="childLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="goodDinosaurTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
        <a class="callToAction">BUY TICKETS</a>
    </div>
    
    
    <div class="quad B L">
        <?php $thisMovie = $foreign; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Alex and Eve Title Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="tamashaLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="tamashaTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
        <a class="callToAction">BUY TICKETS</a>
    </div>
    
    
    <div class="quad B R">
        <?php $thisMovie = $action; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Cover Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="spectreLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="spectreTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
        <a class="callToAction">BUY TICKETS</a>
    </div>
</div>