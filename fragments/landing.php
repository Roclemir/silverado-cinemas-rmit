 <!--Now showing slider -->
<?php
    function printSliderListItems( $arrItems )
    {
        foreach( $arrItems as $details )
        {
            echo '<li><img src="'.$details['sliderPic'].'" alt="'.$details['title'].' ('.$details['rating'].' )" title="'.$details['title'].' ('.$details['rating'].' )">';
        }
    }
    
    function printSliderThumbs( $arrItems )
    {
        foreach( $arrItems as $details )
        {
            echo '<li><img src="'.$details['sliderThumb'].'" alt="'.$details['title'].' ('.$details['rating'].' )" title="'.$details['title'].' ('.$details['rating'].' )" ></li>';
        }
    }
?>
<div class="showing">
    <div class="title">
        <h1>NOW SHOWING</h1>
    </div>
    <div id="amazingslider-wrapper-5" style="display:block;position:relative;max-width:800px;padding-left:0px; padding-right:244px;margin:0px auto 0px;">
        <div id="amazingslider-5" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                
                <li><img src="img/slider/nowshow.jpg" alt="Now Showing"  title="Now Showing" />
                </li>
                <?php
                    printSliderListItems( $showingArray );
                ?>
                <li><img src="img/slider/7GqClqvlObY.jpg" alt="Feature Trailer - <?php echo $action['title']." (".$action['title'].")";  ?>"  title="Feature Trailer - <?php echo $action['title']; ?>(<?php echo $action['rating'];  ?>" />
                <video preload="none" src="https://www.youtube.com/embed/7GqClqvlObY?v=7GqClqvlObY" ></video>
                </li>
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <li><img src="img/slider/nowshow-tn.jpg" alt="Now Showing" title="Now Showing" /></li>
                <?php
                    printSliderThumbs( $showingArray );
                ?>
                <li><img src="img/slider/7GqClqvlObY-tn.jpg" alt="Feature Trailer - <?php echo $action['title']." (".$action['title'].")"; ?>"  title="Feature Trailer - <?php echo $action['title']." (".$action['title'].")"; ?>" /></li>
            </ul>
        <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive Slider jQuery">Responsive Slider jQuery</a></div>
        </div><!-- End amazingslider -->
    </div><!-- End amazingslider-wrapper -->
</div><!-- End showing class -->
    <br>

<!-- Coming Soon slider -->
<div class="coming">
    <div class="title">
        <h1>COMING SOON</h1>
    </div>
    <div id="amazingslider-wrapper-4" style="display:block;position:relative;max-width:800px;padding-left:0px; padding-right:244px;margin:0px auto 0px;">
        <div id="amazingslider-4" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                <li><img src="img/slider/comingSoon.jpg" alt="Coming Soon"  title="Coming Soon" />
                </li>
                <?php
                    printSliderListItems( $comingArray );
                ?>
                <li><img src="img/slider/0P-gn9dZG7s.jpg" alt="Featured - <?php echo $carol['title'].' ('.$carol[rating].')'; ?>"  title="Featured - <?php echo $carol['title'].' ('.$carol[rating].')'; ?>" />
                <video preload="none" src="https://www.youtube.com/embed/0P-gn9dZG7s?v=0P-gn9dZG7s" ></video>
                </li>
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <li><img src="img/slider/comingSoon-tn.jpg" alt="Coming Soon" title="Coming Soon" /></li>
                <?php
                    printSliderThumbs( $comingArray );
                ?>
                <li><img src="img/slider/0P-gn9dZG7s-tn.jpg" alt="Featured - <?php echo $carol['title'].' ('.$carol[rating].')'; ?>" title="Featured - <?php echo $carol['title'].' ('.$carol[rating].')'; ?>" /></li>
            </ul>
        <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive jQuery Image Slideshow">Responsive jQuery Image Slideshow</a></div>
        </div><!-- End amazingslider -->
    </div><!-- End amazingslider-wrapper -->
</div><!-- End coming class -->
<!--    <br>-->