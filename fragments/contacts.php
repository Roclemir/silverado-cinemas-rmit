<h3 class="titleFormat">CONTACT US</h3>
<div class="contactForm">
    <form method="post" action="http://titan.csit.rmit.edu.au/~e54061/wp/testcontact.php">
        <div class="halfForm">
            <fieldset>
                <label for="email">Email:</label>
                <input class="input" type="email" id="email" name="email" required>
            </fieldset>
            <fieldset>
                <label for="subject">Subject:</label>
                <select class="input" id="subject" name="subject" required>
                    <option value="general">General Enquiry</option>
                    <option value="group">Group and Corporate bookings</option>
                    <option value="suggestions">Suggestions and complaints</option>
                </select>
            </fieldset>
            <input type="submit">
        </div>
        <div class="halfForm">
            <fieldset>
                <label for="message">Enquiry details:</label>
                <textarea class="input" name="message" required minlength="20"></textarea>
            </fieldset>
        </div>
    </form>
</div>