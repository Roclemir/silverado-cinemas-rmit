<!-- Referrences:
Movie pictures sourced from Hoytes website:
Hoyts, (n.d.). Hoyts. 
[online] Available at: http://www.hoyts.com.au/ 
[Accessed 23 Dec. 2015].

Movie details sourced from IMDB website:
IMDb, (n.d.). IMDb - Movies, TV and Celebrities. 
[online] Available at: http://www.imdb.com/ 
[Accessed 23 Dec. 2015].
-->

<!-- This page fragment displays movies coming soon to the venue -->

<div class="quadrants">
    
    <!-- The second level div tag classes T L R and B relate to (T)op, (L)eft, 
        (R)ight and (B)ottom. So a div with the classes T and L is in the top 
        left of the page. These classes are used in the CSS to get the desired
        effect. -->
    
    <!-- Each quadrant is layed out is like this:
        Name of movie at the top,
        Div containing the cover picture on the left and some basic details in
        a few divs on the right,
        A one-liner blurb and
        A link to watch the trailer.
    -->
        
    
    <div class="quad T L">
        <?php $thisMovie = $csForeign; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Title Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="csForeignLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="sistersTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
    </div>
    
    <div class="quad T R">
        <?php $thisMovie = $csChild; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Alex and Eve Title Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="csChildLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="peanutsTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
    </div>
    
    
    <div class="quad B L">
        <?php $thisMovie = $csRomcom; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Cover Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="csRomcomLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="carolTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
    </div>
    
    
    <div class="quad B R">
        <?php $thisMovie = $csAction; ?>
        <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
        <div class="details">
            <a class="alex&EveLink moreDetails">
                <img class="movieImg shImg" src="<?php echo $thisMovie['thumb']; ?>" 
                     alt="Cover Picture">
            </a>
            <div class="labels">
                <p>Cast:</p>
                <p>
                    <?php
                        $actors = $thisMovie['cast'];
                        $actor = strtok( $actors, INLINE_DELIM );
                        while( $actor !== false )
                        {
                            echo $actor.'<br>';
                            $actor = strtok( INLINE_DELIM );
                        }
                    ?>
            </p>
            </div>
            <div class="labels">
                <p>Director:</p>
                <p><?php echo $thisMovie['direct']; ?></p>
            </div>
            <div class="labels">
                <p>Genre:</p>
                <p><?php echo $thisMovie['genre']; ?></p>
            </div>
            <div class="labels">
                <p>Length:</p>
                <p><?php echo $thisMovie['length']; ?> Mins</p>
            </div>
            <div class="labels">
                <p>Rating:</p>
                <p><?php echo $thisMovie['rating']; ?></p>
            </div>
            <div>
                <a class="csActionLink moreDetails">More Details</a>
            </div>
        </div>
        <p>A mixture of cultures provides a comedic clash when two lives crash together in this 
            romantic comedy directed by Peter Andrikidis.
        </p>
        <a class="trailer">WATCH TRAILER</a>
        <iframe id="creedTrailer" class="hide" width="560" height="315" 
                src="<?php echo $thisMovie['trailerURL']; ?>" allowfullscreen></iframe>
    </div>
</div>



