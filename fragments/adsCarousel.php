                <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 150px; overflow: hidden; visibility: hidden;">
                     <!--Loading Screen -->
                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 1000px; height: 100%;"></div>
                        <div style="position:absolute;display:block;background:url('slider/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                    </div>
                     <!--End loading screen div -->
                    
                    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 150px; overflow: hidden;">
                        
                         <!--Slide 1 -->
                        <div data-p="112.50" style="display: none;">
                            <img data-u="image" src="../img/ads/coke.jpg" />
                            <div data-u="caption" data-t="0" style="position: absolute; top: 350px; left: 368px; width: 250px; height: 30px; background-color: rgba(255,255,255,0); font-size: 40px; color: #ffffff; line-height: 30px; text-align: center;">Grab a coke.</div>
                        </div>
                         <!--End Slide 1 -->
                        
                         <!--Slide 2 -->
                        <div data-p="112.50" style="display: none;">
                            <img data-u="image" src="../img/ads/popcorn.jpg" />
                            <div data-u="caption" data-t="1" data-3d="1" style="position: absolute; top: -120px; left: 600px; width: 350px; height: 30px; background-color: rgba(255,40,40,1); font-size: 20px; color: #ffffff; line-height: 30px; text-align: center;">Enjoy your movie with popcorn!</div>
                        </div>
                        <div data-p="112.50" style="display: none;">
                            <img data-u="image" src="../img/ads/choctop.jpg" />
                            <div data-u="caption" data-t="2" style="position: absolute; top: 5px; left: -372px; width: 270px; height: 30px; background-color: rgba(235,81,0,0.5); font-size: 20px; color: #ffffff; line-height: 30px; text-align: center;">Don't forget a choc top or two!</div>
                        </div>
            
                    </div>
                     <!--Bullet Navigator -->
                    <div data-u="navigator" class="jssorb01" style="bottom:5px;right:485px;">
                        <div data-u="prototype" style="width:12px;height:12px;"></div>
                    </div>
                     <!--Arrow Navigator -->
                    <span data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
                    <span data-u="arrowright" class="jssora02r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"></span>
                    <a href="http://www.jssor.com" style="display:none">Slideshow Maker</a>
                </div>