<div id="buyWrapper">
    <div class="buyForm">
        <div class="buy L">
            <h3 class="titleFormat">Ticketing Info</h3>
<!-- The action attribute needs to be changed - this is just for testing -->
            <form id="purchase" method="post" action="index.php">
                <fieldset class="purchase">
                    <label for="datePicker">First, select a date:</label>
                    <input type="date" id="datePicker" name="booking[date]">
                </fieldset>
                <fieldset class="movieField hide">
                    <label for="selectedMovie">Next, select a movie:</label>
                    <select id="selectedMovie" name="booking[movie]">
                        <option id="default" disabled selected> -- select a movie -- </option>
                        <option id="selectTamasha" value="AF">Tamasha</option>
                        <option id="selectDino" value="CH">The Good Dinosaur</option>
                        <option id="selectAlexEve" value="RC">Alex &amp; Eve</option>
                        <option id="selectSpectre" value="AC">Spectre</option>
                    </select>
                </fieldset>
                
                <fieldset class="tixField">
                    <h3 id="delux">DELUXE SEATS</h3>
                    <label for="sAdult">Adults:
                    <select id="sAdult" class="tickets" name="booking[seats][SA]">
                    </select></label>
                    <label for="sChild">Children:
                    <select id="sChild" class="tickets" name="booking[seats][SC]">
                    </select></label>
                    <label for="sConc">Concession:
                    <select id="sConc" class="tickets" name="booking[seats][SP]">
                    </select> </label>
                </fieldset>
                <fieldset class="tixField">
                    <h3 id="firstclass">FIRST CLASS SEATS</h3>
                    <label for="pAdult">Adults:
                    <select id="pAdult" class="tickets" name="booking[seats][FA]">
                    </select></label>
                    <label for="pChild">Children:
                    <select id="pChild" class="tickets" name="booking[seats][FC]">
                    </select></label>
                </fieldset>
                <fieldset class="tixField">
                    <h3 id="beanbags">BEANBAGS</h3>
                    <label for="bean1">For 1:
                    <select id="bean1" class="tickets" name="booking[seats][B1]">
                    </select></label>
                    <label for="bean2">For 2:
                    <select id="bean2" class="tickets" name="booking[seats][B2]">
                    </select></label>
                    <label for="bean3">For 3:
                    <select id="bean3" class="tickets" name="booking[seats][B3]">
                    </select> </label>
                </fieldset>
                <input type="hidden" class="dayOfMovie" name="booking[day]" value="">
                <input type="submit" class="submitPurch hide" value="ADD TO CART">
            </form>
        </div> <!-- End .buy .L -->
        <div class="buy R">
            <h3 class="titleFormat">Selected Info</h3>
            <p id="selectAdate">Select A date on the left</p>
        </div>
        <p style="clear:both;">NB: Discounted prices (if applicable) are displayed on the cart page</p>
    </div>
    
    <div class="clear">
        <br>
        <a id="layoutLink" onclick="$.featherlight( $( '.layout' ) )">Check out our new theatre layout</a>
        <div class="hide layout"><img src="img/Theatre.png"></div>
    </div>
    
    <div class="keepTogether">
        <div class="priceTimes L">
            <h3 class="titleFormat">Pricing Information</h3>
            <!-- Table layout for the pricing info -->
            <div class="table priceTable">
                <div class="headRow">
                    <div class="cell">
                        <p>Price List</p>
                    </div>
                    <div class="cell">
                        <p>Mon - Tue (All Day)</p>
                        <p>Wed - Fri (1pm only)</p>
                    </div>
                    <div class="cell">
                        <p>Wed - Fri (not 1pm)</p>
                        <p>Sat - Sun (All Day)</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>Standard Full Price</p>
                    </div>
                    <div class="cell">
                        <p>$12</p>
                    </div>
                    <div class="cell">
                        <p>$18</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>Standard Concession</p>
                    </div>
                    <div class="cell">
                        <p>$10</p>
                    </div>
                    <div class="cell">
                        <p>$15</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>Standard Child</p>
                    </div>
                    <div class="cell">
                        <p>$8</p>
                    </div>
                    <div class="cell">
                        <p>$12</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>First Class Adult</p>
                    </div>
                    <div class="cell">
                        <p>$25</p>
                    </div>
                    <div class="cell">
                        <p>$30</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>First Class Child</p>
                    </div>
                    <div class="cell">
                        <p>$20</p>
                    </div>
                    <div class="cell">
                        <p>$25</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>Beanbag*</p>
                    </div>
                    <div class="cell">
                        <p>$20</p>
                    </div>
                    <div class="cell">
                        <p>$30</p>
                    </div>
                </div> 
            </div> <!-- End pricing table -->
            <p>*Beanbags are for up to two adults or three children</p>
        </div>

        <div class="priceTimes R">
            <h3 class="titleFormat">Times</h3>
            <!-- Table layout for movie times -->
            <div class="table timeTable">
                <div class="headRow">
                    <div class="cell">
                        <p>Mon - Tue</p>
                    </div>
                    <div class="cell">
                        <p>Wed - Fri</p>
                    </div>
                    <div class="cell">
                        <p>Sat - Sun</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>1pm - The Good Dinosaur</p>
                    </div>
                    <div class="cell">
                        <p>1pm - Alex &amp; Eve</p>
                    </div>
                    <div class="cell">
                        <p>12pm - The Good Dinosaur</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>6pm - Tamasha</p>
                    </div>
                    <div class="cell">
                        <p>6pm - The Good Dinosaur</p>
                    </div>
                    <div class="cell">
                        <p>3pm - Tamasha</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p>9pm - Alex &amp; Eve</p>
                    </div>
                    <div class="cell">
                        <p>9pm - Spectre</p>
                    </div>
                    <div class="cell">
                        <p>6pm - Alex &amp; Eve</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">
                        <p></p>
                    </div>
                    <div class="cell">
                        <p></p>
                    </div>
                    <div class="cell">
                        <p>9pm - Spectre</p>
                    </div>
                </div>
            </div><!-- End Times table -->
        </div>
    </div><!-- End .priceTimes .R -->
</div> <!-- End #buyWrapper -->