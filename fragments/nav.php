<nav>
    <ul> <!-- Using javascript onclick() function for these "links" 
              hence the abbreviated <a> tags -->
        <li class="listItem current"><a>HOME</a></li>
        <li class="listItem"><a>NOW SHOWING</a></li>
        <li class="listItem"><a>COMING SOON</a></li>
        <li class="listItem"><a>RESERVE TICKETS</a></li>
        <li class="listItem"><a>SHOPPING CART</a></li>
        <li class="listItem"><a>CONTACT US</a></li>
    </ul>
</nav>