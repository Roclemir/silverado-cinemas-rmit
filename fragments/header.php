<header>
    <h1 class="capital cinemaName">S</h1>
    <div class="titleWrapper">
        <h1 class="cinemaName">ilverado</h1>
        <h1 class="subtitle">Cinemas</h1>
    </div>
    <div class="dolby">
        <h2 class="nowWith">now with</h2>
        <img src="img/dolbySparkle.gif" alt="Dolby Symbol">
        <h2 class="nowWith">Surround Sound</h2>
    </div>
    <div class="clear"></div>
</header>