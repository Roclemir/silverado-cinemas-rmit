<footer>
        <div class="frow">
            <div class="fcell">
                <a href="http://titan.csit.rmit.edu.au/~s3575400/wp/" target="_blank">
                    <p>Student name: Ryan Couper<br>
                    Student number: s3575400</p>
                </a>
                <a href="http://titan.csit.rmit.edu.au/~s3550163/wp/" target="_blank">
                    <p>Student name: Nicolas Slater<br>
                    Student number: s3550163</p>
                </a>
            </div>
            <div class="fcell">
                <p>This website has been created as part of an assignment in an 
                    approved course of study for RMIT University and contains 
                    copyright material not created by the author. All copyright
                    material used remains copyright of the respective owners and
                    has been used here pursuant to Section 40 of the Copyright Act
                    1968 (Commonwealth of Australia). No part of this work may be
                    reproduced without consent of the original copyright owners.
                    See code comments for references.</p>
            </div>
        </div> <!-- end table row -->
</footer>