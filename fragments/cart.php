<div class="title">
    <h1>RESERVE YOUR TICKETS</h1>
</div>
<script>
    var post = <?php echo json_encode( $_POST['booking'] ); ?>;
    var postVouch = <?php echo json_encode( $_POST['voucher'] ); ?>;
    if( post || postVouch )
    {
        $( '#home' ).addClass( 'hide' );
        $( '#cart' ).removeClass( 'hide' );
    }
</script>
<?php
    
    /* Define some seeting codes for readability */
    define( 'STAND_ADULT', 'SA' );
    define( 'STAND_CHILD', 'SC' );
    define( 'STAND_PNSNR', 'SP' );
    define( 'FIRST_ADULT', 'FA' );
    define( 'FIRST_CHILD', 'FC' );
    define( 'BEANBAG_ONE', 'B1' );
    define( 'BEANBAG_TWO', 'B2' );
    define( 'BEANBAG_THR', 'B3' );
    $regexPattern = '/^[0-9]{5}[-]{1}[0-9]{5}[-]{1}[a-zA-Z]{2}$/';
    
    /* Check for correct voucher code */
    if( isset( $_POST['voucher'] ) )
    {   /* Voucher code entered */
        if( preg_match( $regexPattern, $_POST['voucher'] ) )
        {   /* Regular expression matched */
            $str    = $_POST['voucher'];
            $first  = strtok( $str, "-" );
            $second = strtok( "-" );
            $hash   = strtok( "-" );
            
            if( checkHash( $first, $second, $hash ) )
            {
                $_SESSION['discount'] = true;
            }
        }
    }
    
    function checkHash( $first, $second, $hash )
    {
        /* Ascii value of any lowercase letter minus the ascii value of 'a' will
         * give us a number between 0 and 26 */
        $hash1 = ord( strtolower( $hash[0] ) ) - ord('a');
        $hash2 = ord( strtolower( $hash[1] ) ) - ord('a');
        
        $sum1 = formulaSum( $first, 5 );
        $sum2 = formulaSum( $second, 5 );
        
        if( $sum1 == $hash1 && $sum2 == $hash2 )
        {
            return true;
        }
        return false;
    }
    
    /* A function that works out the secret formula for the hash code. sssshhhh!! */
    function formulaSum( $nums, $len )
    {
        $sum = ( ( ( $nums[0] * $nums[1] + $nums[2]) * $nums[3] + $nums[4] ) ) % 26;
        return $sum;
    }
    
    if( isset( $_POST['booking'] ) )
    {
        $_SESSION['screening'][] = $_POST['booking'];
    }
    
    function getCost( $screening, $code, $num )
    {
        /* Sets the cost of the ticket per person */
        if( ( $time === 1 
            && ( $screening['day'] != "Saturday"
                 && $screening['day'] != "Sunday" ) )
            || ( $screening['day'] == "Monday")
            || ( $screening['day'] == "Tuesday") )
        {
            switch( $code )
            {
                case STAND_ADULT:
                    $cost = 12;
                    break;
                case STAND_CHILD:
                    $cost = 8;
                    break;
                case STAND_PNSNR:
                    $cost = 10;
                    break;
                case FIRST_ADULT:
                    $cost = 25;
                    break;
                case FIRST_CHILD:
                    $cost = 20;
                    break;
                case BEANBAG_ONE:
                case BEANBAG_TWO:
                case BEANBAG_THR:
                    $cost = 20;
            }
        } else {
            switch( $code )
            {
                case STAND_ADULT:
                    $cost = 18;
                    break;
                case STAND_CHILD:
                    $cost = 12;
                    break;
                case STAND_PNSNR:
                    $cost = 15;
                    break;
                case FIRST_ADULT:
                    $cost = 30;
                    break;
                case FIRST_CHILD:
                    $cost = 25;
                    break;
                case BEANBAG_ONE:
                case BEANBAG_TWO:
                case BEANBAG_THR:
                    $cost = 30;
            }
        } /* End cost setting cost */
        
        /* Apply discount if code valid code entered */
        if( $_SESSION['discount'] === true )
        {
            $cost = $cost * 0.8; /* 80% */
        }
        return $cost;
    }

    /* This next chukn is the cart display area */
    function displayInfo() {
        if( isset( $_SESSION['screening'] ) )
        {
            foreach( $_SESSION['screening'] as $screening )
            {
                /* Get the title and time of the movie */
                switch( $screening['movie'] ) {
                    case ACTION:
                        $title = $action['title'];
                        $time = 9;
                        break;
                    case CHILD:
                        $title = $child['title'];
                        switch( $screening['day'] ) {
                            case "Monday":
                            case "Tuesday":
                                $time = 1;
                                break;
                            case "Wednesday":
                            case "Thursday":
                            case "Friday":
                                $time = 6;
                                break;
                            case "Saturday":
                            case "Sunday":
                                $time = 12;
                        }
                        break;
                    case FOREIGN:
                        $title = $foreign['title'];
                        switch( $screening['day'] ) {
                            case "Monday":
                            case "Tuesday":
                                $time = 6;
                                break;
                            case "Saturday":
                            case "Sunday":
                                $time = 3;
                        }
                        break;
                    case ROMCOM:
                        $title = $romcom['title'];  
                        switch( $screening['day'] ) {
                            case "Monday":
                            case "Tuesday":
                                $time = 9;
                                break;
                            case "Wednesday":
                            case "Thursday":
                            case "Friday":
                                $time = 1;
                                break;
                            case "Saturday":
                            case "Sunday":
                                $time = 6;
                        }
                        break;
                } /* end of getting title and time */
                
                /* Display the data */
                echo '<div class="cartWrapper">';
                    echo '<div class="cartTable">';
                        echo '<div class="cartRow">';
                            echo '<div class="cartCell">';
                                echo 'Movie:';
                            echo '</div>'; /* cartCell */
                            echo '<div class="cartCell">';
                                switch( $screening['movie'] ) {
                                    case 'AC':
                                        echo $action['title'];
                                        break;
                                    case 'CH':
                                        echo $child['title'];
                                        break;
                                    case 'AF':
                                        echo $foreign['title'];
                                        break;
                                    case 'RC':
                                        echo $romcom['title'];
                                        break;
                                }
                            echo '</div>'; /* .cartCell */
                        echo '</div>'; /* .cartRow */
                        echo '<div class="cartRow">';
                            echo '<div class="cartCell">';
                                echo 'Day';
                            echo '</div>'; /* .cartCell */
                            echo '<div class="cartCell">';
                                echo $screening['day'];
                            echo '</div>'; /* .cartCell */
                        echo '</div>'; /* .cartRow */
                        foreach( $screening['seats'] as $code => $num )
                        {
                            if( $num != 0 )
                            {
                                $cost = getCost( $screening, $code, $num );
                                /* Set display labels */
                                switch( $code )
                                {
                                    case STAND_ADULT:
                                        $label = "adult seats";
                                        break;
                                    case STAND_CHILD:
                                        $label = "child seats";
                                        break;
                                    case STAND_PNSNR:
                                        $label = "pension discount seats";
                                        break;
                                    case FIRST_ADULT:
                                        $label = "first class adult seats";
                                        break;
                                    case FIRST_CHILD:
                                        $label = "first class child seats";
                                        break;
                                    case BEANBAG_ONE:
                                    case BEANBAG_TWO:
                                    case BEANBAG_THR:
                                        $label = "bean bag(s)";
                                } /* End setting display label */
                                
                                /* Total cost for number of tickets */
                                $totCost = $cost * $num;
                                
                                echo '<div class="cartRow">';
                                    echo '<div class="cartCell">';
                                        echo $num.' '.$label;
                                    echo '</div>'; /* .cartCell */
                                    echo '<div class="cartCell">';
                                        echo '$ '.number_format( $totCost, 2 );
                                    echo '</div>'; /* .cartCell */
                                echo '</div>'; /* .cartRow */
                                if( isset( $finalCost ) ) {
                                    $finalCost += $totCost;
                                } else {
                                    $finalCost = $totCost;
                                }
                                
                            } /* end if( $nums != 0 ) */
                        } /* End foreach loop */
                    echo '</div>'; /* .cartTable */
                echo '</div>'; /* .cartWrapper */
            }
        echo '<div class="cartTable">';
            echo '<div class="cartRow">';
                echo '<div class="cartCell">';
                    echo 'Total cost';
                echo '</div>'; /* .cartCell */
                echo '<div class="cartCell">';
                    echo number_format( $finalCost, 2 );
                echo '</div>'; /* .cartCell */
            echo '</div>'; /* .cartRow */
        echo '</div>'; /* .cartTable */
        } else {   /* What to display for an empty cart */
            echo "<p>There's nothing in your cart!</p>";
        }
    }
    displayInfo();
?>
<!-- End main information display -->

<!-- Discount voucher code below -->
<br>
<div class="voucher">
    <form action="index.php" method="post">
        <fieldset>
            <label for="voucher">Voucher Code:</label>
            <input class="code" id="txtboxToFilter" pattern="[0-9]{5}[-]{1}[0-9]{5}[-]{1}[a-zA-Z]{2}" maxlength="14" type="text" id="code" name="voucher" placeholder="12345-67890-TK" required>
            <input type='submit' value='Apply' />
        </fieldset>
    </form>
</div>
<div>
    <p>(Dashes are added automatically)</p>
</div>
<!-- End discount voucher code -->

<!-- Details input code below -->
<h2 class=titleFormat>Your Details</h2>
<br>
<div class="detailForm">
    <form action="index.php" method="post">
        <fieldset>
            <label for="name">Name:</label>
            <input class="name" minlength="2" type="name" id="userName" name="details[name]" required>
        </fieldset>
            
        <fieldset>
            <label for="phone">Phone:</label>
            <input class="phone" pattern="[0-9]{8,10}" type="phone" id="userPhone" name="details[phone]" required>
        </fieldset>
        
        <fieldset>
            <label for="email">Email:</label>
            <input class="email" type="email" id="userEmail" name="details[email]" required>
        </fieldset>
        
        <input type="submit" class="reserve" value="CONFIRM RESERVATION"/>
    </form>
</div>
<br>
<!-- End user details code -->
<!--
     ."".    ."",
     |  |   /  /
     |  |  /  /
     |  | /  /
     |  |/  ;-._ 
     }  ` _/  / ;
     |  /` ) /  /
     | /  /_/\_/\
     |/  /      |
     (  ' \ '-  |
      \    `.  /
       |      |
-->