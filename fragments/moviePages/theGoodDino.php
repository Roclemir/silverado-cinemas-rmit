<!-- References:
Disney Australia Movies,. 2015. "The Good Dinosaur".
http://movies.disney.com.au/the-good-dinosaur.
-->

<div class="individual">
    <!-- This next line makes sure all the details on this page are for the 
    same movie. To change the movie, change this next line -->
    <?php $thisMovie = $child; ?>
    <h3><?php echo strtoupper( $thisMovie['title'] ); ?></h3>
    <img src="<?php echo $thisMovie['pic']; ?>" alt="Cover Picure" class="bigPic">
    <div class="indPageDetails">
        <p>Cast:</p>
        <p>
            <?php
                $actors = $thisMovie['cast'];
                $actor = strtok( $actors, INLINE_DELIM );
                while( $actor !== false )
                {
                    echo $actor.'<br>';
                    $actor = strtok( INLINE_DELIM );
                }
            ?>
        </p>
    </div>
    <div class="indPageDetails">
        <p>Director:</p>
        <p><?php echo $thisMovie['direct']; ?></p>
    </div>
    <div class="indPageDetails">
        <p>Genre:</p>
        <p><?php echo $thisMovie['genre']; ?></p>
    </div>
    <div class="indPageDetails">
        <p>Length:</p>
        <p><?php echo $thisMovie['length']; ?> mins</p>
    </div>
    <div class="indPageDetails">
        <p>Rating:</p>
        <p><?php echo $thisMovie['rating']; ?></p>
    </div>
    <div class="indPageDetails">
        <p>Producers:</p>
        <p>
            <?php
                $producers = $thisMovie['producer'];
                $producer = strtok( $producers, INLINE_DELIM );
                while( $producer !== false )
                {
                    echo $producer.'<br>';
                    $producer = strtok( INLINE_DELIM );
                }
            ?>
        </p>
    </div>
    <a class="trailer">WATCH TRAILER</a>
    <div class="indSynopsis">
        <h4>Synopsis:</h4>
        <p><?php echo $thisMovie['synopsis']; ?>
        </p>
    </div>
    <a class="callToAction">BUY TICKETS</a>
</div>
<div class="times">
    <span>Movie times:</span>
    <span>Mon/Tue - 1pm</span>
    <span>Wed/Thur/Fri - 6pm</span>
    <span>Sat/Sun - 12pm</span>
</div>