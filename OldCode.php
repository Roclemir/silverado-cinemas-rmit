        <div class="movie">
                            <div class="midAlign">
                                <h3><?php echo strtoupper( $rcGenre ); ?></h3>
                                <a class="alex&EveLink moreDetails">
                                    <img class="movieImg land" src="<?php echo $rcThumb ?>" 
                                        alt="Image of current romance movie">
                                </a>
                                <p class="mvtitle"><?php echo strtoupper( $rcTitle )." (".$rcRating.")"; ?></p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3><?php echo strtoupper( $chGenre ); ?></h3>
                                <a id="" class="goodDinoLink moreDetails">
                                    <img class="movieImg land" src="<?php echo $chThumb; ?>" 
                                         alt="Image of current childrens movie">
                                </a>
                                <p class="mvtitle"><?php echo strtoupper( $chTitle )." (".$chRating.")"; ?></p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3><?php echo strtoupper( $afGenre ); ?></h3>
                                <a class="tamashaLink moreDetails">
                                    <img class="movieImg land" src="<?php echo $afThumb; ?>" 
                                         alt="Image of current art/foriegn movie">
                                </a>
                               <p class="mvtitle"><?php echo strtoupper( $afTitle )." (".$afRating.")"; ?></p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3><?php echo strtoupper( $acGenre ); ?></h3>
                                <a class="spectreLink moreDetails">
                                    <img class="movieImg land" src="<?php echo $acThumb; ?>" 
                                         alt="Image of current action movie">
                                </a>
                                <p class="mvtitle"><?php echo strtoupper( $acTitle )." (".$acRating.")"; ?></p>
                            </div>
                        </div> 
                        
                    </div> <!-- end movies div -->
                </div> <!-- end showing div -->
                
                 End Coming Soon slider 
                
                

                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3>COMEDY</h3>
                                <a class="sistersLink moreDetails">
                                    <img class="movieImg land" src="img/thumbs/Sis-thumb.jpg" 
                                         alt="Image of new romance movie">
                                </a>
                                <p class="mvtitle">SISTERS (MA)</p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3>CHILDREN</h3>
                                <a class="peanutsLink moreDetails">
                                    <img class="movieImg land" src="img/thumbs/Sno-thumb.jpg" 
                                         alt="Image of new childrens movie">
                                </a>
                                <p class="mvtitle">THE PEANUTS MOVIE (G)</p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3>ROMANCE</h3>
                                <a class="carolLink moreDetails">
                                    <img class="movieImg land" src="img/thumbs/Car-thumb.jpg" 
                                         alt="Image of new art/foriegn movie">
                                </a>
                                <p class="mvtitle">CAROL (M)</p>
                            </div>
                        </div>
                        
                        <div class="movie">
                            <div class="midAlign">
                                <h3>ACTION</h3>
                                <a class="creedLink moreDetails">
                                    <img class="movieImg land" src="img/thumbs/creed-thumb.jpg" 
                                         alt="Image of new action movie">
                                </a>
                                <p class="mvtitle">CREED (M)</p>
                            </div>
                        </div>
                        
                    </div> <!-- end movies div --> 
                </div> <!-- end coming div -->
                
        <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:800px;margin:0px auto 64px;">
            <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                <ul class="amazingslider-slides" style="display:none;">
                <li><img src="images/nowshow.jpg" alt="Now Showing"  title="Now Showing" />
                </li>
                <li><img src="<?php echo $acSliderPic; ?>" alt="<?php echo $acTitle.' ('.$acRating,')'; ?>"  title="<?php echo $acTitle.' ('.$acRating,')'; ?>" />images/spec.jpg" alt="Spectre (M)"  title="Spectre (M)" />
                </li>
                <li><img src="<?php echo $rcSliderPic; ?>" alt="<?php echo $rcTitle.' ('.$rcRating,')'; ?>"  title="<?php echo $rcTitle.' ('.$rcRating,')'; ?>" />
                </li>
                <li><img src="<?php echo $chSliderPic; ?>" alt="<?php echo $chTitle.' ('.$chRating,')'; ?>"  title="<?php echo $chTitle.' ('.$chRating,')'; ?>" />
                </li>
                <li><img src="<?php echo $afSliderPic; ?>" alt="<?php echo $afTitle.' ('.$afRating,')'; ?>"  title="<?php echo $afTitle.' ('.$afRating,')'; ?>" />Tamasha (PG)"  title="Tamasha (PG)" />
                </li>
                <li><img src="images/7GqClqvlObY.jpg" alt="Featured - Spectre (M)"  title="Featured - Spectre (M)" />
                <video preload="none" src="https://www.youtube.com/embed/7GqClqvlObY?v=7GqClqvlObY" ></video>
                </li>
                </ul>
                <ul class="amazingslider-thumbnails" style="display:none;">
                <li><img src="images/nowshow-tn.jpg" alt="Now Showing" title="Now Showing" /></li>
                <li><img src="images/spec-tn.jpg" alt="Spectre (M)" title="Spectre (M)" /></li>
                <li><img src="images/alex-tn.jpg" alt="Alex and Eve (MA15+)" title="Alex and Eve (MA15+)" /></li>
                <li><img src="images/dino-tn.jpg" alt="The Good Dinosaur (G)" title="The Good Dinosaur (G)" /></li>
                <li><img src="images/tam-tn.jpg" alt="Tamasha (PG)" title="Tamasha (PG)" /></li>
                <li><img src="images/7GqClqvlObY-tn.jpg" alt="Featured - Spectre (M)" title="Featured - Specre (M)" /></li>
                </ul>
                <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive JavaScript Image Slider">Responsive JavaScript Image Slider</a></div>
            </div>
        </div> <!-- End carousel 
    </div><!-- End movies class 
</div><!-- End showing class 

<!--    <div class="movies">-->
<!--        <div id="amazingslider-wrapper-2" style="display:block;position:relative;max-width:800px;margin:0px auto 98px;">-->
<!--            <div id="amazingslider-2" style="display:block;position:relative;margin:0 auto;">-->
<!--                <ul class="amazingslider-slides" style="display:none;">-->
<!--                    <li><img src="images-soon/comingSoon.jpg" alt="Coming Soon"  title="Coming Soon" />-->
<!--                    </li>-->
<!--                    <li><img src="images-soon/creedSlide.jpg" alt="Creed (M)"  title="Creed (M)" />-->
<!--                    </li>-->
<!--                    <li><img src="images-soon/carol.jpg" alt="Carol (M)"  title="Carol (M)" />-->
<!--                    </li>-->
<!--                    <li><img src="images-soon/peanuts.jpg" alt="The Peanuts Movie (G)"  title="The Peanuts Movie (G)" />-->
<!--                    </li>-->
<!--                    <li><img src="images-soon/sisters.jpg" alt="Sisters (MA)"  title="Sisters (MA)" />-->
<!--                    </li>-->
<!--                    <li><img src="images-soon/0P-gn9dZG7s.jpg" alt="Featured - Carol (M)"  title="Featured - Carol (M)" />-->
<!--                    <video preload="none" src="https://www.youtube.com/embed/0P-gn9dZG7s?v=0P-gn9dZG7s" ></video>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <ul class="amazingslider-thumbnails" style="display:none;">-->
<!--                    <li><img src="images-soon/comingSoon-tn.jpg" alt="Coming Soon" title="Coming Soon" /></li>-->
<!--                    <li><img src="images-soon/creedSlide-tn.jpg" alt="Creed (M)" title="Creed (M)" /></li>-->
<!--                    <li><img src="images-soon/carol-tn.jpg" alt="Carol (M)" title="Carol (M)" /></li>-->
<!--                    <li><img src="images-soon/peanuts-tn.jpg" alt="The Peanuts Movie (G)" title="The Peanuts Movie (G)" /></li>-->
<!--                    <li><img src="images-soon/sisters-tn.jpg" alt="Sisters (MA)" title="Sisters (MA)" /></li>-->
<!--                    <li><img src="images-soon/0P-gn9dZG7s-tn.jpg" alt="Featured - Carol (M)" title="Featured - Carol (M)" /></li>-->
<!--                </ul>-->
<!--                <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive Slider jQuery">Responsive Slider jQuery</a></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

         <!--Free version of "Amazing Slider" is being  -->
         <!--Now Showing plugin -->
        <script src="NSengine/jquery.js"></script>
        <script src="NSengine/amazingslider.js"></script>
        <link rel="stylesheet" type="text/css" href="NSengine/amazingslider-5.css">
        <script src="NSengine/initslider-5.js"></script>
        
         <!--Coming Soon plugin -->
        <script src="CSengine/jquery.js"></script>
        <script src="CSengine/amazingslider.js"></script>
        <link rel="stylesheet" type="text/css" href="CSengine/amazingslider-4.css">
        <script src="CSengine/initslider-4.js"></script>
