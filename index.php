<!-- This first chunk of php checks if the user has confirmed their reservation and 
if they have, saves the details to a file and erases the session data.
It had to be here to prevent data being loaded into a session post-confirmation, 
otherwise the session from pre-confirmation would be loaded at session_start(); -->
<?php
    session_start();
    /* This php saves the reservation to a file */
    if( isset( $_POST['details'] ) )
    {
        /* Open a file to save to */
        $filename = "reservations.txt";
        $file = fopen( $filename, "a" );
        
        /* Setup details for printing to file */
        $details = uniqid() . ',' . implode( ',', $_POST['details'] );
        foreach( $_SESSION['screening'] as $screening )
        {
            $details .= $screening['date'].',';
            $details .= $screening['movie'].',';
            if( isset( $_SESSION['discount'] ) && $_SESSION['discount'] === true )
            {
                $details .= "true,";
            } else {
                $detials .= "false,";
            }
            foreach( $screening['seats'] as $code => $num )
            {
                $details .= $code.':'.$num.',';
            }
        }
        
        /* Add end of line character to the info */
        $details .= PHP_EOL;
        
        fwrite( $file, $details );
        fclose( $file );
        session_destroy();
    }
    
    /* Above and below this line were originally two seperate chunks of php */
    
    define( 'ACTION', 'AC' );
    define( 'CHILD',  'CH' );
    define( 'FOREIGN','AF' );
    define( 'ROMCOM', 'RC' );
    define( 'INLINE_DELIM', '|' );
    
    $childFile   = "child.txt";
    $actionFile  = "action.txt";
    $foreignFile = "foreign.txt";
    $romcomFile  = "romcom.txt";
    $csActionFile = "csAction.txt";
    $csChildFile = "csChild.txt";
    $csForeignFile = "csForeign.txt";
    $csRomcomFile = "csRomcom.txt";
    
    function getMvDetails( $fileName ) {
        $count = 0;
        $file = fopen( $fileName, "rb" );
        $line = fgetcsv( $file );
        $arr['title']        = $line[ $count++ ];
        $arr['cast']         = $line[ $count++ ];
        $arr['direct']       = $line[ $count++ ];
        $arr['producer']     = $line[ $count++ ]; 
        $arr['genre']        = $line[ $count++ ]; 
        $arr['length']       = $line[ $count++ ];
        $arr['rating']       = $line[ $count++ ];
        $arr['shortDesc']    = fgets( $file );
        $arr['synopsis']     = fgets( $file );
        $line = fgetcsv( $file );
        $count = 0;
        $arr['pic']          = $line[ $count++ ];
        $arr['thumb']        = $line[ $count++ ];
        $arr['sliderPic']    = $line[ $count++ ];
        $arr['sliderThumb']  = $line[ $count++ ];
        $arr['trailerURL']   = fgets( $file );
        
        /* Some of the above could have been done in a loop, but I wanted to create an associative array */
        /* Interesting point, I first attempted the above using strtok to split each line with "," as the 
        *  delimeter, but it wasn't tokenizing the lines correctly, I was getting splits on spaces and sometimes
        *  it would miss a comma altogether, so I chose to use fgetcsv instead, which worked perfectly */
        
        fclose( $file );
        return $arr;
    }
    
    /* Load the now showing movie files into variables */
    $romcom     = getMvDetails( $romcomFile     );
    $action     = getMvDetails( $actionFile     );
    $foreign    = getMvDetails( $foreignFile    );
    $child      = getMvDetails( $childFile      );
    /* Stick them in an array */
    $showingArray = array( $romcom, $action, $foreign, $child );
    
    /* Repeat for the coming soon movies */
    $csRomcom   = getMvDetails( $csRomcomFile   );
    $csAction   = getMvDetails( $csActionFile   );
    $csChild    = getMvDetails( $csChildFile    );
    $csForeign  = getMvDetails( $csForeignFile  );
    $comingArray = array( $csRomcom, $csAction, $csForeign, $csChild );
?>
<!DOCTYPE html>
<!-- 
All images remain the property of their respective copywright owners.
Image references:
Mental Floss, (2016). The Missing Links: A History of Movie Popcorn. 
[online] Available at: http://mentalfloss.com/article/49116/missing-links-history-movie-popcorn 
[Accessed 3 Jan. 2016].

PETER SCHAFRICK I LIQUIDS PHOTOGRAPHER I DIRECTOR, (2016). cola splash. 
[online] Available at: http://www.peterschafrick.com/STILLS/-BEVERAGES-&-BOTTLES/13/ 
[Accessed 3 Jan. 2016].

Coke
Pinterest.com, (2016). 
[online] Available at: https://www.pinterest.com/skid47/christmas-catering-ideas-carnival-food/ 
[Accessed 3 Jan. 2016].

Modification to images has been done by Nicolas Slater.
-->
<html>
    <head>
        <title>Silverado Cinemas</title>
        <meta charset="UTF-8">
        <!-- It is recommended for anyone trying to follow the javascript 
        that you begin with the control.js file as it contains info on
        what files the functions are in. -->
        
        <!-- jQuery and jQuery UI referrences:
        jquery.org, jQuery. 2016. "Datepicker | Jquery UI". 
        Jqueryui.Com. Accessed January 3. 
        https://jqueryui.com/datepicker/.

        jquery.org, jQuery. 2016. "Jquery UI". Jqueryui.Com. 
        Accessed January 3. 
        https://jqueryui.com/.
        -->
        <link type="text/css" rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.structure.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.structure.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.theme.min.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
        
        <!-- Form validation with jQuery plugin:
        Jqueryvalidation.org,. 2016. 
        "Jquery Validation Plugin | Form Validation With Jquery". 
        Accessed January 3. http://jqueryvalidation.org/.
        -->
        <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
        <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
        <!-- fallback local libraries -->
        <script>
            if( !($.validator) ) {
                document.write('<script type="text/javascript" src="js/jquery.validate.min.js"><\/script>');
                document.write('<script type="text/javascript" src="js/additional-methods.min.js"><\/script>');
            }
            if ( !window.jQuery )   {
                document.write('<link type="text/css" rel="stylesheet" src="css/jquery-ui.structure.min.css">');
                document.write('<link type="text/css" rel="stylesheet" src="css/jquery-ui.theme.min.css">');
                document.write('<script type="text/javascript" src="js/jquery-1.11.3.min.js"><\/script>');
                document.write('<script type="text/javascript" src="js/jquery-ui.min.js"><\/script>');
                document.write('<script type="text/javascript" src="js/jquery.validate.min.js"><\/script>');
            }
        </script>
        <!-- Javascript files written for this site specifically -->
        <script type="text/javascript" src="js/contentChange.js"></script>
        <script type="text/javascript" src="js/trailers.js"></script>
        <script type="text/javascript" src="js/control.js"></script>
        <script type="text/javascript" src="js/purchaseTix.js"></script>
        <script type="text/javascript" src="js/bookmark.js"></script>
        
        <!-- The Featherlight plugin gives the effect of Mac's lightbox to the 
        vider/photo/html element that is attached to it. In the site, it is used
        for watching the video trailers -->
        <!-- Featherlight plugin referrence:
        Bossart, Noel. 2016. "Featherlight – The Ultra Slim Jquery Lightbox.". 
        Noelboss.Github.Io. 
        Accessed January 1. http://noelboss.github.io/featherlight/.
        -->
        <!-- Featherlight plugin css file -->
        <link href="css/featherlight.min.css" type="text/css" rel="stylesheet">
        
        <!-- Jssor.com, (n.d.). Image Slider - Jssor Slider. 
            [online] Available at: http://www.jssor.com/demos/image-slider.slider 
            [Accessed 24 Feb. 2016].
        -->
        
        <!-- Browser width specific css files -->
        <link type="text/css" rel="stylesheet" href="css/main.css">
        
        <!-- Bookmark button css -->
        <link rel="stylesheet" href="css/addtohomescreen.css">
        
        <!-- Amazingslider.com,. Amazing Slider | jQuery Slider, 
        WordPress Slideshow, jQuery Video Gallery. 
        Retrieved 27 February 2016, 
        from https://amazingslider.com -->
        
        <!--Now Showing plugin -->
        <!--<script src="NSengine/jquery.js"></script>-->
        <script src="NSengine/amazingslider.js"></script>
        <link rel="stylesheet" type="text/css" href="NSengine/amazingslider-5.css">
        <script src="NSengine/initslider-5.js"></script>
        
         <!--Coming Soon plugin -->
        <!--<script src="CSengine/jquery.js"></script>-->
        <script src="CSengine/amazingslider.js"></script>
        <link rel="stylesheet" type="text/css" href="CSengine/amazingslider-4.css">
        <script src="CSengine/initslider-4.js"></script>
        
    </head>
    <body>
        <img src="img/popcorn.gif" alt="" class="bottomLeftImage">
        <img src="img/chocTop.gif" alt="" class="bottomLeftImage2">
        <img src="img/cokeGlass.gif" alt="" class="bottomRightImage">
        <!-- background video -->
        <!-- IE 9 and below will not recognise this video tag and just show the
            background picture -->
        <div class="fullscreen-bg">
            <video poster="img/bg/curtainsBG.jpg" autoplay loop 
                   class="fullscreen_video" preload="auto">
                <source src="movie/AllTrailers.mp4" type="video/mp4">
            </video>
        </div>
        <div class="wrapper">
            <?php require 'fragments/header.php';?>
            <?php require 'fragments/nav.php';?>
<!-- Main content area - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
            <div class="mainBody">
<!-- These next fragments load the entire contents of the site. This may take a
litle time on first visit however the customer base is primarily made up of
repeat customers and once cached locally, this site will load and switch pages
extremely quickly, only slowing down (very slightly) when a new movie has 
been added. -->
                <!-- These fragments contain the main pages as on the Nav Bar -->
                
                <div id="home"                   ><?php require 'fragments/landing.php'    ;?></div>
                <div id="showing"    class="hide"><?php require 'fragments/showing.php'    ;?></div>
                <div id="coming"     class="hide"><?php require 'fragments/coming.php'     ;?></div>
                <div id="buy"        class="hide"><?php require 'fragments/buy.php'        ;?></div>
                <div id="contacts"   class="hide"><?php require 'fragments/contacts.php'   ;?></div>
                <!-- These next fragments are the unique movie content -->
                <!-- The now showing movie content -->
                <div id="alexEve"       class="hide"><?php require 'fragments/moviePages/alexEve.php'    ;?></div>
                <div id="theGoodDino"   class="hide"><?php require 'fragments/moviePages/theGoodDino.php';?></div>
                <div id="tamasha"       class="hide"><?php require 'fragments/moviePages/tamasha.php'    ;?></div>
                <div id="spectre"       class="hide"><?php require 'fragments/moviePages/spectre.php'    ;?></div>
                <!-- The coming soon movie content -->
                <div id="sisters"       class="hide"><?php require 'fragments/moviePages/sisters.php'    ;?></div>
                <div id="peanuts"       class="hide"><?php require 'fragments/moviePages/peanuts.php'    ;?></div>
                <div id="carol"         class="hide"><?php require 'fragments/moviePages/carol.php'      ;?></div>
                <div id="creed"         class="hide"><?php require 'fragments/moviePages/creed.php'      ;?></div>
                <!-- Shopping cart divs -->
                <div id="cart"          class="hide"><?php require 'fragments/cart.php'                  ;?></div>
                
            </div> <!-- end mainBody div -->
            <!-- Footer -->
            <?php require 'fragments/footer.php';?>
            <!-- Bookmark button -->
            <br><br>
            <button id="bookmark-this">Bookmark This Page</button>
            
            <!-- Test form code -->
            
            <!-- End form code -->
        </div> <!-- end wrapper div -->
        <!-- Featherlight plugin javascript file (referrence/details at top of page) 
             It has been placed here as this is where the author recommended putting it-->
        <script type="text/javascript" src="js/featherlight.min.js"></script>
    </body>
</html>

